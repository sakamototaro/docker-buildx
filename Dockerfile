FROM docker:19.03.6
ENV DOCKER_BUILDKIT=1
RUN mkdir -p ~/.docker/cli-plugins/ && \
    wget https://github.com/docker/buildx/releases/download/v0.3.1/buildx-v0.3.1.linux-amd64 && \
    mv buildx-v0.3.1.linux-amd64 ~/.docker/cli-plugins/docker-buildx && \
    chmod a+x ~/.docker/cli-plugins/docker-buildx
